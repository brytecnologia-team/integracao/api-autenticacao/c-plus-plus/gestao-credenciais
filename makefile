include ./makedefs

MODELO_SRC_FILES = $(wildcard $(MODELO)/*.cpp)
MODELO_OBJ_FILES = $(patsubst $(MODELO)/%.cpp, $(MODELO)/%.o, $(MODELO_SRC_FILES))
UTIL_SRC_FILES   = $(wildcard $(UTIL)/*.cpp)
UTIL_OBJ_FILES   = $(patsubst $(UTIL)/%.cpp, $(UTIL)/%.o, $(UTIL_SRC_FILES))
SERVICE_SRC_FILES = $(wildcard $(SERVICE)/*.cpp)
SERVICE_OBJ_FILES = $(patsubst $(SERVICE)/%.cpp, $(SERVICE)/%.o, $(SERVICE_SRC_FILES))
OBJ_FILES        = $(MODELO_OBJ_FILES) $(UTIL_OBJ_FILES) $(SERVICE_OBJ_FILES) 
	
all: aplicacao backend

aplicacao: $(OBJ_FILES) $(SRC)/aplicacao.o
	$(CXX) -o $@ $(OBJ_FILES) $(SRC)/aplicacao.o $(CXXFLAGS)

backend: $(OBJ_FILES) $(SRC)/backend.o
	$(CXX) -o $@ $(OBJ_FILES) $(SRC)/backend.o $(CXXWEBFLAGS)
	
%.o: %.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS)

$(OBJ_FILES):
	$(MAKE) -C $(MODELO)
	$(MAKE) -C $(SERVICE)
	$(MAKE) -C $(UTIL)

.PHONY: clean

clean:
	$(MAKE) -C $(MODELO) clean
	$(MAKE) -C $(SERVICE) clean
	$(MAKE) -C $(UTIL) clean
	rm -f $(SRC)/*.o aplicacao core $(SRC)/*~ $(INCLUDE)/*~
	rm -f $(SRC)/*.o backend core $(SRC)/*~ $(INCLUDE)/*~
	