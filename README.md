# Gestão de validade da credencial de acesso

Este projeto apresenta uma abordagem de gerenciamento do tempo de vida das credenciais emitidas pela plataforma da BRy ([BRy Cloud]).

A abordagem apresenta um gerenciamento ativo, onde uma tarefa realiza validações períodicas do período de vigência do access token armazenado em memória.

O access token armazenado em memória válido e monitorado é disponibilizado na forma de serviço através do endereço: http://localhost:8080/

Abaixo seguem os passos principais:
* Ao iniciar, a aplicação exemplo requisita um **Access Token** para a plataforma de serviços da BRy, utilizando as credenciais de uma aplicação cadastrada: **client_id** e **client_secret**.  
* Em um período configurável, a tarefa desperta e realiza a validação do tempo de vida do Access Token (processo de decodifição do JWT e extração da claim **exp**).
* Sempre que o Access Token estiver próximo de sua expiração, a tarefa dispara o evento de renovação da credencial utilizando o **Refresh Token**.

### Tech

O exemplo utiliza as seguintes tecnologias e bibliotecas C++ abaixo:
* [Pistache]        - Pistache para desenvolvimento backend
* [curl/Curl.h]     - Biblioteca para transferencia de dados
* [json-c/json.h]   - Biblioteca para dados JSON
* [c++17]           - Compilador C++17.

### Variáveis que devem ser configuradas

O exemplo necessita ser configurado com os dados de conexão e identificação da aplicação cliente.

| Variável | Descrição | Arquivo |
| ------ | ------ | ------ |
| URL_SERVER | Endereço do endpoint de obtenção de credenciais. | include/config/service_config.h
| CLIENT_ID | **client_id** de uma aplicação. | include/config/client_config.h
| CLIENT_SECRET | **client_secret** de uma aplicação. | include/config/client_config.h


### Uso

Para execução escolha sua IDE de preferência ou execute por linha de comando, executando os comando abaixo na pasta do projeto:
    make
    ./backend

 [BRy Cloud]: <https://cloud.bry.com.br>