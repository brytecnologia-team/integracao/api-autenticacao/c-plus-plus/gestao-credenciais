#ifndef __expired_refresh_token_exception_h
#define __expired_refresh_token_exception_h

#include <exception>

class ExpiredRefreshTokenException: public std::exception
{
public:
    virtual const char* what() const throw() override
    {
      return "Envio de refresh_token inválido (expirado).";
    }
};

#endif