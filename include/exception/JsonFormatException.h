#ifndef __json_format_exception_h
#define __json_format_exception_h

#include <exception>
#include <string>

class JsonFormatException: public std::exception
{
typedef std::string String;

private:
    String error_message;

public:
    JsonFormatException() {
      error_message = "Formato JSON diferente do esperado.";
    }

    JsonFormatException(String msg) {
      error_message = "Formato JSON diferente do esperado: " + msg;
    }
    
    virtual const char* what() const throw()
    {
      return error_message.c_str();
    }
};

#endif