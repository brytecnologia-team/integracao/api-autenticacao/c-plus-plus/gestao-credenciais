#ifndef __post_service_exception_h
#define __post_service_exception_h

#include <exception>
#include <string>

class PostServiceException: public std::exception
{
typedef std::string String;

private:
    String error_message;

public:
    PostServiceException(String msg) {
      error_message = "Um erro aconteceu ao tentar realizar POST: " + msg;
    }
    
    virtual const char* what() const throw()
    {
      return error_message.c_str();
    }
};

#endif