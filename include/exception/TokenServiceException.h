#ifndef __token_service_exception_h
#define __token_service_exception_h

#include <exception>
#include <string>

class TokenServiceException: public std::exception
{
typedef std::string String;

private:
    String status;
    String message;

    String error_message;
 
public:   
    virtual const char* what() const throw()
    {
        return error_message.c_str();
    }
 
public:       
    TokenServiceException(String status, String message)
    {
        this -> status = status;
        this -> message = message;
        this -> error_message = "Status:" + status + ", Message:" + message;
    }
    String getStatus() { return status; }
    String getMessage() {return message; }
};

#endif