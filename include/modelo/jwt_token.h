#ifndef __JWTTOKEN_H
#define __JWTTOKEN_H

#include <string>

#include "tempo_restante.h"

class JWTToken {
typedef std::string String;

private:
    String access_token;
    String refresh_token;
    String token_type;
    String not_before_policy;
    String session_state;
    String scope;

    //Data e Horário da expiração
    TempoRestante * tempo_restante_token;
    TempoRestante * tempo_restante_refresh_token;

public:
    JWTToken(String acces_token, String refresh_token, 
            String expires_in, String refresh_expires_in, 
            String token_type, String not_before_policy, 
            String sesession_state, String scope);
    ~JWTToken();

    String get_access_token();
    String get_refresh_token();
    String get_token_type();
    String get_not_before_policy();
    String get_session_state();
    String get_scope();

    double get_tempo_expiracao_token();
    double get_tempo_expiracao_refresh_token();
};

#endif