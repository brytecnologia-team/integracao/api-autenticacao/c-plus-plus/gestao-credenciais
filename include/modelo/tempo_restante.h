#ifndef __TEMPORESTANTE_H
#define __TEMPORESTANTE_H

#include <string>
#include <chrono>
#include <ctime>   

class TempoRestante {
typedef std::string String;
typedef std::time_t Tempo;

private:
    Tempo tempo_expiracao;

public:
    TempoRestante(String expires_in);
    double get_tempo_restante();
};

#endif