#ifndef __SCHEDULER_H
#define __SCHEDULER_H

#include "service/SchedulerTaskService.h"

#include <thread>
#include <string>
#include <unistd.h>
#include <iostream>

class Scheduler {
typedef std::thread Thread;
typedef std::exception Exception;

private:
    SchedulerTaskService * schedulerService;
    Thread thread;

public:
    Scheduler(TokenService * tokenService, Logger * logger);

    void schedule();
    void start();
    void join();
};

#endif