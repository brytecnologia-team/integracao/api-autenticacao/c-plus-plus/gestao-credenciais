#ifndef __SCHEDULER_TASK_SERVICE_H
#define __SCHEDULER_TASK_SERVICE_H

#include "config/service_config.h"
#include "service/TokenService.h"
#include "service/Logger.h"

class SchedulerTaskService {

typedef std::exception Exception;
private:
    bool token_verbose;
    long check_interval = VERIFY_REFRESH_RATE;
    TokenService * token_service;
    Logger * logger;

public:
    SchedulerTaskService(TokenService * token_service, Logger * logger, bool token_verbose = false);
    void checkCredentialExpiration();
    void initializeApplicationCredential();
};

#endif