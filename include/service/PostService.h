#ifndef __POST_SERVICE_H
#define __POST_SERVICE_H

#include <string>
#include <stdexcept>
#include <curl/curl.h>
#include <json-c/json.h>

#include "util/http_util.h"
#include "exception/JsonFormatException.h"
#include "exception/PostServiceException.h"

class PostService {
typedef std::string String;

private:
    CURL * curl;
    CURLcode res;
    struct curl_slist *list = NULL;
	String * response = new String("");
    String post = "";

    String urlFormat(String value);

public:
    PostService();
    ~PostService();
    void perform();
    void setURL(String url);
    void addHeader(String key, String value);
    void addBody(String key, String value);

    json_object * getJsonResponse();
    long getHttpCode();
};

#endif