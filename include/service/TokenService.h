#ifndef __TOKEN_SERVICE_H
#define __TOKEN_SERVICE_H

#include <string>

#include "PostService.h"
#include "modelo/jwt_token.h"
#include "config/client_config.h"
#include "config/service_config.h"
#include "util/jwt_token_from_json.h"
#include "util/error_message_from_json.h"
#include "exception/ExpiredRefreshTokenException.h"

class TokenService {

typedef std::string String;
private:
    String clientSecret = CLIENT_SECRET;
    String clientId = CLIENT_ID;
    String endereco = URL_SERVER;
    JWTToken * tokenAtual;

public:
    ~TokenService();
    JWTToken * generateAccessToken();
    JWTToken * renewAccessToken();
    JWTToken * postAccessTokenRequest();
    JWTToken * postRenewRequest(String refreshToken);
};




#endif