#ifndef __LOGGER_H
#define __LOGGER_H

#include <iostream>
#include <string>

using namespace std;

class Logger {

typedef string String;
public:
    void error(String mensagem_erro);
    void info(String mensagem_info);
    void token(String token);
};

#endif