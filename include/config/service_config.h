#ifndef __service_config_h
#define __service_config_H

#define URL_SERVER "https://cloud-hom.bry.com.br/token-service/jwt"

//Tempo em segundos para cada verificação
#define VERIFY_REFRESH_RATE 60

#endif
