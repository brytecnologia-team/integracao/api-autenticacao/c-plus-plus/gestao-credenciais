#ifndef __http_util_h
#define __http_util_h

#include <string>

size_t get_response_body(void * ptr, size_t size, size_t nmemb, void * userp);

#endif
