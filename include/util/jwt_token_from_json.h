#ifndef __jwt_token_from_json_h
#define __jwt_token_from_json_h

#include "modelo/jwt_token.h"
#include "exception/JsonFormatException.h"

#include <string>
#include <json-c/json.h>

JWTToken * jwt_token_from_json(json_object * json);

#endif