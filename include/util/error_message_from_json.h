#ifndef __error_message_from_json_h
#define __error_message_from_json_h

#include "exception/JsonFormatException.h"
#include "exception/TokenServiceException.h"

#include <string>
#include <json-c/json.h>

std::exception * error_message_from_json(json_object * json);

#endif