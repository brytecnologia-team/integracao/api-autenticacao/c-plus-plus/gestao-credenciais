#ifndef __json_string_from_jwt_token_h
#define __json_string_from_jwt_token_h

#include "modelo/jwt_token.h"

#include <string>

std::string json_string_from_jwt_token(JWTToken * token);

#endif