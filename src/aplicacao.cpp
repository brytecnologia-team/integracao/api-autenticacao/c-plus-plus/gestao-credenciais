#include "service/SchedulerTaskService.h"
#include "config/service_config.h"

#include <unistd.h>
#include <string>
#include <iostream>

int main()
{
    if (std::string(CLIENT_ID).compare("<CLIENT_ID>") == 0) {
        std::cout << "Cliente ID não configurado" << std::endl;
        return 1;
    } else if (std::string(CLIENT_SECRET).compare("<CLIENT_SECRET>") == 0) {
        std::cout << "Cliente Secret não configurado" << std::endl;
        return 2;
    }
    Logger * logger = new Logger();
    TokenService * tokenService = new TokenService();
    SchedulerTaskService * schedulerService = new SchedulerTaskService(tokenService, logger, true);

    try {
        schedulerService -> initializeApplicationCredential();
    
        while(true) {
            sleep(VERIFY_REFRESH_RATE);

            schedulerService -> checkCredentialExpiration();
        }
    } catch(std::exception * e) {
        logger -> error(e -> what());
        logger -> info("Saindo da aplicação");

        delete e;
        return 3;
    } catch(...) {
        logger -> error("Exceção desconhecida!");

        return 4;
    }

    return 0;
}