#include "util/error_message_from_json.h"

std::exception * error_message_from_json(json_object * json)
{
    std::string status, message;

    json_object * json_status = json_object_object_get(json, "status_code");
    json_object * json_message = json_object_object_get(json, "message");

    if (!(json_status && json_message))
        return new JsonFormatException();

    status = json_object_get_string(json_status);
    message = json_object_get_string(json_message);

    return new TokenServiceException(status, message);
}