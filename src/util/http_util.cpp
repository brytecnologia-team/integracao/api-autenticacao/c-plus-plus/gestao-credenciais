#include "util/http_util.h"

size_t get_response_body(void * ptr, size_t size, size_t nmemb, void * userp)
{
	std::string extra_data((char *) ptr);
	std::string * response_as_string = (std::string *) *(std::string **)userp;
	response_as_string->append(extra_data);
	return size * nmemb;
}
