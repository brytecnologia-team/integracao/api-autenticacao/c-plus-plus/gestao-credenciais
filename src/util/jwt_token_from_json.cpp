#include "util/jwt_token_from_json.h"

JWTToken * jwt_token_from_json(json_object * json) {
    std::string acces_token, refresh_token, 
            expires_in, refresh_expires_in, 
            token_type, not_before_policy, 
            sesession_state, scope;

    json_object * json_acces_token = json_object_object_get(json, "access_token");
    json_object * json_refresh_token = json_object_object_get(json, "refresh_token");
    json_object * json_expires_in = json_object_object_get(json, "expires_in");
    json_object * json_refresh_expires_in = json_object_object_get(json, "refresh_expires_in");
    json_object * json_token_type = json_object_object_get(json, "token_type");
    json_object * json_not_before_policy = json_object_object_get(json, "not-before-policy");
    json_object * json_sesession_state = json_object_object_get(json, "session_state");
    json_object * json_scope = json_object_object_get(json, "scope");

    if (!(json_acces_token && json_refresh_token && json_expires_in 
                && json_refresh_expires_in && json_token_type && json_not_before_policy
                && json_sesession_state && json_scope))
        throw new JsonFormatException();

    acces_token = json_object_get_string(json_acces_token);
    refresh_token = json_object_get_string(json_refresh_token);
    expires_in = json_object_get_string(json_expires_in);
    refresh_expires_in = json_object_get_string(json_refresh_expires_in);
    token_type = json_object_get_string(json_token_type);
    not_before_policy = json_object_get_string(json_not_before_policy);
    sesession_state = json_object_get_string(json_sesession_state);
    scope = json_object_get_string(json_scope);

    return new JWTToken(acces_token, refresh_token, 
            expires_in, refresh_expires_in, 
            token_type, not_before_policy, 
            sesession_state, scope);
}