#include "util/json_string_from_jwt_token.h"

std::string json_string_from_jwt_token(JWTToken * token) {
    std::string json = "{\n";
    json += "\"access_token\"         : \"" + token -> get_access_token()                                    + "\"\n";
    json += ", \"refresh_token\"      : \"" + token -> get_refresh_token()                                   + "\"\n";
    json += ", \"expires_in\"         : \"" + std::to_string(token -> get_tempo_expiracao_token())           + "\"\n";
    json += ", \"refresh_expires_in\" : \"" + std::to_string(token -> get_tempo_expiracao_refresh_token())   + "\"\n";
    json += ", \"token_type\"         : \"" + token -> get_token_type()                                      + "\"\n";
    json += "}";
     
    return json;
}