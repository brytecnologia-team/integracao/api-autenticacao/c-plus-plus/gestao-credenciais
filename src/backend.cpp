#include "util/json_string_from_jwt_token.h"
#include "service/Scheduler.h"
#include "config/service_config.h"

#include "pistache/endpoint.h"

using namespace Pistache;

class TokenHandler : public Http::Handler
{
private:
    TokenService * tokenService;

public:
    HTTP_PROTOTYPE(TokenHandler)

    void onRequest(const Http::Request& request, Http::ResponseWriter response) override
    {
        JWTToken * token = tokenService -> generateAccessToken();
        std::string json = json_string_from_jwt_token(token);

        response.send(Pistache::Http::Code::Ok, json.c_str());
    }

    TokenHandler(TokenService * tokenService)
    {
        this -> tokenService = tokenService;
    }
};

int main()
{    
    if (std::string(CLIENT_ID).compare("<CLIENT_ID>") == 0) {
        std::cout << "Cliente ID não configurado" << std::endl;
        return 1;
    } else if (std::string(CLIENT_SECRET).compare("<CLIENT_SECRET>") == 0) {
        std::cout << "Cliente Secret não configurado" << std::endl;
        return 2;
    }
    TokenService * tokenService = new TokenService();
    Logger * logger = new Logger();

    Scheduler * scheduler = new Scheduler(tokenService, logger);
    scheduler->start();

    Pistache::Address addr(Pistache::Ipv4::any(), Pistache::Port(8080));
    auto opts = Pistache::Http::Endpoint::options()
                    .threads(1);

    Http::Endpoint server(addr);
    server.init(opts);
    server.setHandler(Http::make_handler<TokenHandler>(tokenService));
    server.serve();

    scheduler->join();
}