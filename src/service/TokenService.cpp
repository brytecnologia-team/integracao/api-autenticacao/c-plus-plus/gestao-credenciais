#include "service/TokenService.h"
JWTToken * TokenService::generateAccessToken() 
{ 
    //Verifica se tokenAtual já está definido
    if (this -> tokenAtual) {
        //Verifica se tokenAtual está expirado
        if (this -> tokenAtual -> get_tempo_expiracao_token() <= 0) {
            JWTToken * antigo = this -> tokenAtual;
            this -> tokenAtual = postAccessTokenRequest();
            delete antigo;
        }
    } else {
        this -> tokenAtual = postAccessTokenRequest();
    }

    return this -> tokenAtual;
}

JWTToken * TokenService::renewAccessToken() 
{   
    //Verifica se já expirou
    if (this -> tokenAtual -> get_tempo_expiracao_refresh_token() <= 0) {
        throw new ExpiredRefreshTokenException();
    }

    JWTToken * antigo = this -> tokenAtual;
    this -> tokenAtual = postRenewRequest(this -> tokenAtual -> get_refresh_token());
    delete antigo;

    return this -> tokenAtual; 
}

JWTToken * TokenService::postAccessTokenRequest() 
{
    //Cria serviço de envio POST
    PostService postService;

    //Adiciona Headers
    postService.addHeader("Content-Type", "application/x-www-form-urlencoded");

    //Adiciona Atributos
    postService.addBody("grant_type", "client_credentials");
    postService.addBody("client_id", clientId);
    postService.addBody("client_secret", clientSecret);

    //Adiciona URL
    postService.setURL(endereco);

    //Executa POST
    postService.perform();

    //Recebe resposta JSON
    json_object * json_object = postService.getJsonResponse();
    
    long http_code = postService.getHttpCode();

    //Ocorreu um erro na requisição
    if (http_code != 200) {
        throw error_message_from_json(json_object);
    }

    JWTToken * novo = jwt_token_from_json(json_object);

    return novo;
}

JWTToken * TokenService::postRenewRequest(String refreshToken) 
{ 
    //Cria serviço de envio POST
    PostService postService;

    //Adiciona Headers
    postService.addHeader("Content-Type", "application/x-www-form-urlencoded");

    //Adiciona Atributos
    postService.addBody("grant_type", "refresh_token");
    postService.addBody("refresh_token", refreshToken);

    //Adiciona URL
    postService.setURL(endereco);

    //Executa POST
    postService.perform();

    //Recebe resposta JSON
    json_object * json_object = postService.getJsonResponse();
    
    long http_code = postService.getHttpCode();

    //Ocorreu um erro na requisição
    if (http_code != 200) {
        throw error_message_from_json(json_object);
    }

    JWTToken * novo = jwt_token_from_json(json_object);

    return novo;
}

TokenService::~TokenService() { if (this -> tokenAtual) delete tokenAtual; }