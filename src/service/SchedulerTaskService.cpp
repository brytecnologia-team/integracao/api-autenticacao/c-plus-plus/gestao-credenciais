#include "service/SchedulerTaskService.h"

SchedulerTaskService::SchedulerTaskService(TokenService * token_service, Logger * logger, bool token_verbose)
{
    this -> token_verbose = token_verbose;
    this -> token_service = token_service;
    this -> logger = logger;
}

void SchedulerTaskService::checkCredentialExpiration()
{
    //Acessa serviço e recupera o token já gerado
    JWTToken * token = this -> token_service -> generateAccessToken();

    //Verifica se o tempo de expiração é menor que o tempo da próxima verificação
    if (token -> get_tempo_expiracao_token() <= check_interval) {
        logger -> info("Começando acesso a renovação de token.");
        try {
            token = this -> token_service -> renewAccessToken();
            if (token_verbose)
                logger -> token(token -> get_access_token());
        } catch (Exception * e) {
            logger -> error(e -> what());
            delete e;
        }
    } else {
        logger -> info("Tempo restante para utilização do token:" + to_string(token -> get_tempo_expiracao_token() / 60) + " minutos.");
    }
}

void SchedulerTaskService::initializeApplicationCredential()
{
    JWTToken * token = this -> token_service -> generateAccessToken();
    if (token_verbose)
        logger -> token(token -> get_access_token());
}