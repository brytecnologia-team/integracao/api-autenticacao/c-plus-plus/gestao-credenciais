#include "service/Scheduler.h"

Scheduler::Scheduler(TokenService * tokenService, Logger * logger) {
    schedulerService = new SchedulerTaskService(tokenService, logger);
}

void Scheduler::schedule()
{
    while (true) {
        sleep(VERIFY_REFRESH_RATE);
        try {
            schedulerService -> checkCredentialExpiration();
        } catch(Exception * e) {
            std::cout << e->what() << std::endl;
            delete e;
            
            return;
        }
    }
}

void Scheduler::start()
{
    schedulerService -> initializeApplicationCredential();
    thread = Thread(&Scheduler::schedule, this);
}

void Scheduler::join()
{
    thread.join();
}