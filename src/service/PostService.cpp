
#include "service/PostService.h"
#include <iostream>
std::string PostService::urlFormat(String value)
{
    String formatedValue = curl_easy_escape(curl, value.c_str(), value.size());
    return formatedValue;
}

PostService::PostService()
{
    curl_global_init(CURL_GLOBAL_ALL);
    curl = curl_easy_init();

    if (!curl) {
        throw new PostServiceException("Erro em curl_easy_init()");
    }
	
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, get_response_body);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*) &response);

    curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
  
}

PostService::~PostService()
{
    curl_easy_cleanup(curl);
    curl_global_cleanup();
    delete response;
}

void PostService::perform() 
{
    if (list) {
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
    }
    if (post.size() > 0) {
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post.c_str());
    }

    res = curl_easy_perform(curl);

    if(res != CURLE_OK) {
        String mensagem = curl_easy_strerror(res);
        mensagem = "curl_easy_perform() failed: " + mensagem;

        throw new PostServiceException(mensagem);
    }

}

void PostService::setURL(String url) 
{
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
}

void PostService::addHeader(String key, String value) 
{
    String valor =  key + ":" + value;
    list = curl_slist_append(list, valor.c_str());
}

void PostService::addBody(String key, String value) 
{
    String valor = urlFormat(key) + "=" + urlFormat(value);
    if (post.size() > 0)
        valor = "&" + valor;

    post = post + valor;
}

json_object * PostService::getJsonResponse() 
{
    json_tokener* tok = json_tokener_new();
    json_object * response_as_json = json_tokener_parse_ex(tok, response -> c_str(), response -> size());
    if (!response_as_json) {
        enum json_tokener_error error = json_tokener_get_error(tok);
        String message = json_tokener_error_desc(error);
        message = "Error no parser json:" + message + "\nResponse:" + response -> c_str();
        throw new JsonFormatException(message.c_str());
    }
    
    return response_as_json;
}

long PostService::getHttpCode()
{
    long http_code = 0;
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);

    return http_code;
}