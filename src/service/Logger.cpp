#include "service/Logger.h"

void Logger::error(String mensagem_erro)
{
    cout << "ERRO:   [" << mensagem_erro.c_str() << "]\n";
}

void Logger::info(String mensagem_info)
{
    cout << "Info:    " << mensagem_info.c_str() << " \n";
}

void Logger::token(String token)
{
    cout << "Token:  \"" << token.c_str() << "\"\n";
}