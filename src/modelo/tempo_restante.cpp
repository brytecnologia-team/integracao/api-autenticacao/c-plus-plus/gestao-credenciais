#include "modelo/tempo_restante.h"

TempoRestante::TempoRestante(String expires_in) {
    double expiracao = std::stof(expires_in);
    tempo_expiracao = std::chrono::system_clock::to_time_t(
                                std::chrono::system_clock::now()) 
                            + (Tempo) expiracao;
}

double TempoRestante::get_tempo_restante() {
    Tempo tempo_agora = std::chrono::system_clock::to_time_t(
                            std::chrono::system_clock::now());

    return tempo_expiracao - tempo_agora;
}