#include "modelo/jwt_token.h"

JWTToken::JWTToken(String acces_token, String refresh_token, 
            String expires_in, String refresh_expires_in, 
            String token_type, String not_before_policy, 
            String sesession_state, String scope) {
    this->access_token = acces_token;
    this->refresh_token = refresh_token;
    this->token_type = token_type;
    this->not_before_policy = not_before_policy;
    this->session_state = session_state;
    this->scope = scope;
    this->tempo_restante_token = new TempoRestante(expires_in);
    this->tempo_restante_refresh_token = new TempoRestante(refresh_expires_in);
};

JWTToken::~JWTToken() {
    delete tempo_restante_token;
    delete tempo_restante_refresh_token;
}

std::string JWTToken::get_access_token() { return access_token; }
std::string JWTToken::get_refresh_token() { return refresh_token; }
std::string JWTToken::get_token_type() { return token_type; }
std::string JWTToken::get_not_before_policy() { return not_before_policy; }
std::string JWTToken::get_session_state() { return session_state; }
std::string JWTToken::get_scope() { return scope; }

double JWTToken::get_tempo_expiracao_token() { return tempo_restante_token -> get_tempo_restante(); }
double JWTToken::get_tempo_expiracao_refresh_token() { return tempo_restante_refresh_token -> get_tempo_restante(); }
